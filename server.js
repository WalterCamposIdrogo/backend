const express = require("express");
const bodyParser = require("body-parser");
const user = require("./routes/Usuario.routes");
const cuenta = require("./routes/Cuenta.routes");
const InitiateMongoServer = require("./config/db");

// Initiate Mongo Server
InitiateMongoServer();

const app = express();

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, token");
  res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
  next();
});

// PORT
const PORT = process.env.PORT || 4000;

// Middleware
app.use(bodyParser.json());

app.get("/", (req, res) => {
  res.json({ message: "API funcionando" });
});

/**
 * Router Middleware
 * Router - /user/*
 * Method - *
 */
app.use("/usuarios", user);
app.use("/cuenta", cuenta);

app.listen(PORT, (req, res) => {
  console.log(`Server Started at PORT ${PORT}`);
});
