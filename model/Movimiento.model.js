const mongoose = require("mongoose");

const MovimientoSchema = mongoose.Schema({
    descripcion: {
        type: String,
        required: true
    },
    monto: {
        type: Number,
        required: true
    },
    createdAt: {
        type: Date,
        default: Date.now()
    }
});

module.exports = mongoose.model("movimientos", MovimientoSchema);