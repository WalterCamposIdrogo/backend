const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const MovimientoSchema = require('./Movimiento.model').schema;

const CuentaSchema = mongoose.Schema({
    usuarioId: {
        type: mongoose.Types.ObjectId,
        required: true
    },
    nroCuenta: {
        type: String,
        required: true
    },
    saldoContable: {
        type: Number,
        required: true
    },
    saldoDisponible: {
        type: Number,
        required: true
    },
    movimientos: [MovimientoSchema],
    createdAt: {
        type: Date,
        default: Date.now()
    }
});

module.exports = mongoose.model("cuentas", CuentaSchema);
