const mongoose = require("mongoose");

const UsuarioSchema = mongoose.Schema({
  usuario: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  contrasena: {
    type: String,
    required: true
  },
  nombres: {
    type: String,
    required: true
  },
  apepat: {
    type: String,
    required: true
  },
  apemat: {
    type: String,
    required: true
  },
  createdAt: {
    type: Date,
    default: Date.now()
  }
});

module.exports = mongoose.model("usuarios", UsuarioSchema);
