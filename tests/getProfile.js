let chai = require("chai");
let chaiHttp = require("chai-http");
let server = require("../server");
const { response } = require("express");

chai.should();

chai.use(chaiHttp);

let token;
let objUsuario = { _id: null, usuario: '44737524', email: 'walter@gmail.com', nombres: 'Walter', apepat: 'Campos', apemat: 'Idrogo', contrasena: '123456' };

describe('Usuarios API', () => {
    it("POST usuarios/signup", (done) => {
        chai.request(server)
            .post("/usuarios/signup")
            .send(objUsuario)
            .end((err, response) => {
                response.should.have.status(200);
                done();
            })
    })

    it("POST usuarios/login", (done) => {
        chai.request(server)
            .post("/usuarios/login")
            .send({ usuario: objUsuario.usuario, contrasena: objUsuario.contrasena })
            .end((err, response) => {
                response.should.have.status(200);
                token = response.body.token;
                objUsuario._id = response.body.user._id;
                done();
            })
    })

    it("GET cuenta", (done) => {
        chai.request(server)
            .get("/cuenta/" + objUsuario._id)
            .set('token', token)
            .end((err, response) => {
                response.should.have.status(200);
                done();
            })
    })

    it("GET usuarios", (done) => {
        chai.request(server)
            .get("/usuarios")
            .set('token', token)
            .end((err, response) => {
                response.should.have.status(200);
                done();
            })
    })

    it("PUT usuarios", (done) => {
        chai.request(server)
            .put("/usuarios/" + objUsuario._id)
            .set('token', token)
            .send({ nombres: 'Walter Fernando' })
            .end((err, response) => {
                response.should.have.status(200);
                done();
            })
    })

    it("DELETE usuarios", (done) => {
        chai.request(server)
            .delete("/usuarios/" + objUsuario._id)
            .set('token', token)
            .end((err, response) => {
                response.should.have.status(200);
                done();
            })
    })
});