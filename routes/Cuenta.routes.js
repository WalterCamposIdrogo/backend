const express = require("express");
const router = express.Router();
const auth = require("../middleware/auth");

const Cuenta = require("../model/Cuenta.model");

/**
 * @method - GET
 * @description - Cuenta de un usuario
 * @param - /cuenta/:id
 */

router.get("/:id", auth, async (req, res) => {
    try {
        var ObjectId = require('mongodb').ObjectID;
        const cuentas = await Cuenta.find({ usuarioId: ObjectId(req.params.id) });

        res.json(cuentas);
    } catch (e) {
        console.log(e);
        res.send({ message: "Error al recuperar usuario" });
    }
});

module.exports = router;