const express = require("express");
const { check, validationResult } = require("express-validator/check");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const router = express.Router();
const auth = require("../middleware/auth");

const Usuario = require("../model/Usuario.model");
const Cuenta = require("../model/Cuenta.model");
const Movimiento = require("../model/Movimiento.model");

/**
 * @method - POST
 * @param - /signup
 * @description - Registro de usuarios
 */

router.post(
  "/signup",
  [
    check("usuario", "Ingrese un nombre de usuario.")
      .not()
      .isEmpty(),
    check("email", "Ingrese un correo electrónico.").isEmail(),
    check("contrasena", "La contraseña debe tener al menos 6 caracteres.").isLength({
      min: 6
    }),
    check("nombres", "Ingrese nombres.")
      .not()
      .isEmpty(),
    check("apepat", "Ingrese apellido paterno.")
      .not()
      .isEmpty(),
    check("apemat", "Ingrese apellido materno.")
      .not()
      .isEmpty()
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        errors: errors.array()
      });
    }

    const { usuario, email, contrasena, nombres, apepat, apemat } = req.body;
    try {
      let usuarioModel = await Usuario.findOne({
        usuario
      });
      if (usuarioModel) {
        return res.status(400).json({
          msg: "El usuario ya existe!"
        });
      }

      usuarioModel = new Usuario({
        usuario,
        email,
        contrasena,
        nombres,
        apepat,
        apemat
      });

      const salt = await bcrypt.genSalt(10);
      usuarioModel.contrasena = await bcrypt.hash(contrasena, salt);

      await usuarioModel.save();

      for (var x = 1; x < (Math.floor(Math.random() * 10) + 5); x++) {
        usuarioId = usuarioModel.id, nroCuenta = "0011-0057-" + (Math.floor((Math.random() * 9900000000) + 1000000000)), saldoContable = Math.floor(Math.random() * 1000), saldoDisponible = saldoContable;

        cuentaModel = new Cuenta({
          usuarioId,
          nroCuenta,
          saldoContable,
          saldoDisponible
        });

        for (var y = 1; y < (Math.floor(Math.random() * 15) + 5); y++) {
          movimientoModel = new Movimiento({
            descripcion: `Compra en tienda ${x} del producto ${y}`,
            monto: Math.floor(Math.random() * 1000) * -1
          });

          cuentaModel.movimientos.push(movimientoModel);
        }

        await cuentaModel.save();
      }

      const payload = {
        user: {
          id: usuarioModel.id
        }
      };

      jwt.sign(
        payload,
        "randomString",
        {
          expiresIn: 10000
        },
        (err, token) => {
          if (err) throw err;
          res.status(200).json({
            token
          });
        }
      );
    } catch (err) {
      console.log(err.message);
      res.status(500).send("Error al guardar");
    }
  }
);

/**
 * @method - POST
 * @param - /signup
 * @description - Inicio de sesión
 */

router.post(
  "/login",
  [
    check("usuario", "Ingrese usuario")
      .not()
      .isEmpty(),
    check("contrasena", "Ingrese contraseña").isLength({
      min: 6
    })
  ],
  async (req, res) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(400).json({
        errors: errors.array()
      });
    }

    const { usuario, contrasena } = req.body;
    try {
      let user = await Usuario.findOne({
        usuario
      });
      if (!user)
        return res.status(400).json({
          message: "El usuario ingresado no existe"
        });

      const isMatch = await bcrypt.compare(contrasena, user.contrasena);
      if (!isMatch)
        return res.status(400).json({
          message: "Contraseña incorrecta"
        });

      const payload = {
        user: {
          id: user.id
        }
      };

      user.contrasena = null;

      jwt.sign(
        payload,
        "randomString",
        {
          expiresIn: 3600
        },
        (err, token) => {
          if (err) throw err;
          res.status(200).json({
            token,
            user
          });
        }
      );
    } catch (e) {
      res.status(500).json({
        message: "Error en el servidor"
      });
    }
  }
);

/**
 * @method - GET
 * @description - Mi información 
 * @param - /user/me
 */

router.get("/me", auth, async (req, res) => {
  try {
    // request.user is getting fetched from Middleware after token authentication
    const user = await Usuario.findById(req.user.id);
    res.json(user);
  } catch (e) {
    res.send({ message: "Error al recuperar usuario" });
  }
});

/**
 * @method - GET
 * @description - Listado general de usuarios
 * @param - /user/
 */

router.get("/", auth, async (req, res) => {
  try {
    const user = await Usuario.find();
    res.json(user);
  } catch (e) {
    res.send({ message: "Error al recuperar usuarios" });
  }
});

/**
 * @method - GET
 * @description - Datos de un usuario en particular
 * @param - /user/:id
 */

router.get("/:id", auth, async (req, res) => {
  try {
    const user = await Usuario.findById(req.params.id);
    res.json(user);
  } catch (e) {
    res.send({ message: "Error al recuperar usuario" });
  }
});

/**
 * @method - PUT
 * @description - Actualizar usuario
 * @param - /user/:id
 */

router.put("/:id", auth, async (req, res) => {
  if (Object.keys(req.body).length == 0) {
    return res.status(400).send({
      message: "Los datos a actualizar no pueden estar vací­os!"
    });
  }

  const id = req.params.id;

  Usuario.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then(data => {
      if (!data) {
        res.status(404).send({
          message: `Error al  actualizar el usuario =${id}. verifica los parámetros de entrada`
        });
      } else res.send({ message: "El usuario se actualizó correctamente." });
    })
    .catch(err => {
      res.status(500).send({
        message: "Error al actualizar el usuario con id=" + id
      });
    });
});

/**
 * @method - DEL
 * @description - Eliminar usuario
 * @param - /user/:id
 */

router.delete("/:id", auth, async (req, res) => {
  const id = req.params.id;

  Usuario.findByIdAndRemove(id, { useFindAndModify: false })
    .then(data => {
      if (!data) {
        res.status(404).send({
          message: `Error al  eliminar el usuario =${id}. verifica los parámetros de entrada`
        });
      } else {
        res.send({
          message: "Usuario fue eliminado con éxito!"
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Error al eliminar el usuario con id=" + id
      });
    });
});

module.exports = router;
